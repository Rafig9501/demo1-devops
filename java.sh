#!/bin/bash

sudo apt update --fix-missing -y && sudo apt install openjdk-14-jdk -y

export MYSQL_PASS="1Q2W3E4r5tzxc@"
export MYSQL_URL="jdbc:mysql://db:3306/demo1"
export MYSQL_USER="demo1"

function clone_pull {
    Dir=$(basename "$1" .git)
    if [[ -d "$Dir" ]]; then
      cd $Dir
      git pull
    else
      git clone "$1" && cd $Dir
    fi
}

clone_pull https://gitlab.com/Rafig9501/demo1-devops.git
#git clone https://gitlab.com/Rafig9501/demo1-devops.git && cd "$(basename "$_" .git)"

chmod +x mvnw

./mvnw clean package

java -Dspring.profiles.active=mysql -jar target/*.jar
